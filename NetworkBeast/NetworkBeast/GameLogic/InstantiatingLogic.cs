﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using NetworkBeast.NetworkSending;
using NetworkBeastData;

namespace NetworkBeast.GameLogic
{
    class InstantiatingLogic
    {
        public static void InstantiatePlayer(Socket socket, PlayerData playerData, PacketType type)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ServerPacket.Instantiate);
            buffer.WriteString(playerData.GameToken);
            buffer.WriteInt(playerData.CharID);
            buffer.WriteString(playerData.Username);
            buffer.WriteBytes(DataConverters.ConvertVector3ToBytes(playerData.Position));

            switch (type)
            {
                case PacketType.SendToOne:
                    NetworkSender.SendToOne(buffer, socket);
                    break;
                case PacketType.SendToAll:
                    NetworkSender.SendToAll(buffer);
                    break;
                case PacketType.SendToAllExcept:
                    NetworkSender.SendToAllExcept(buffer,socket);
                    break;
            }
        }
    }
}
