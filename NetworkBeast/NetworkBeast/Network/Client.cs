﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using NetworkBeastData;

namespace NetworkBeast.Network
{
    class Client
    {
        private readonly Server m_Server;
        private readonly byte[] m_Buffer;

        public string HashToken { get; set; }

        public Socket Socket { get; }
        public PlayerData PlayerData { get; }

        public Client(Socket socket, Server server)
        {
            m_Server = server;
            Socket = socket;
            m_Buffer = new byte[4096];
            PlayerData = new PlayerData();
            Socket.BeginReceive(m_Buffer, 0, m_Buffer.Length, SocketFlags.None, new AsyncCallback(OnReceiveCallback),
                Socket);
        }

        private void OnReceiveCallback(IAsyncResult ar)
        {
            try
            {
                int readBytes = Socket.EndReceive(ar);

                if (readBytes <= 0)
                {
                    Console.WriteLine("Received  0 bytes?");
                    CloseConnection();
                    return;
                }

                byte[] tempBuff = new byte[readBytes];
                Buffer.BlockCopy(m_Buffer, 0, tempBuff, 0, readBytes);

                if (readBytes > 0)
                    GetPacketsOutOfStream(tempBuff);

                Socket.BeginReceive(m_Buffer, 0, m_Buffer.Length, SocketFlags.None,
                    new AsyncCallback(OnReceiveCallback), Socket);
            }
            catch
            {
                Console.WriteLine("Received some kind of error?");
                CloseConnection();
            }
        }

        private void GetPacketsOutOfStream(byte[] buffer)
        {
            int readPos = 0;
            int totalBytes = buffer.Length;
            while (readPos < totalBytes)
            {
                int packetLength = BitConverter.ToInt32(buffer, readPos) + sizeof(int);
                byte[] data = new byte[packetLength];
                Buffer.BlockCopy(buffer, readPos, data, 0, packetLength);

                m_Server.PacketHandler.QueuePackage(data, Socket);

                readPos += packetLength;
            }
        }

        private void CloseConnection()
        {
            Socket.Close();
            m_Server.DisconnectPlayer(Socket);

        }
    }
}
