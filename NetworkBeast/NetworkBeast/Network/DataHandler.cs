﻿using System;
using System.Collections.Generic;
using System.Text;
using NetworkBeast.PackageHandlers;

namespace NetworkBeast.Network
{
    class DataHandler
    {
        public delegate void Packet(ClientNetworkPackage package);
        private Dictionary<int, Packet> m_Packets;
        public Dictionary<int, Packet> Packets { get => m_Packets; set => m_Packets = value; }

        private LoginHandler m_LoginHandler;
        
        public DataHandler()
        {
            LogDebug.LoadDebug(this);
            m_Packets = new Dictionary<int, Packet>();
            m_LoginHandler = new LoginHandler();
            SetupNetworkMessages();
        }

        private void SetupNetworkMessages()
        {
            Packets.Add((int)ClientPacket.Debug, new DebugHandler().HandleDebug);
            Packets.Add((int)ClientPacket.Login, LoginHandler.Instance.HandleLogin);
            LogDebug.DebugMessage(m_Packets.Count + " - Packet(s) Have been loaded succesfully", LogDebug.MessageType.Status);
        } 
    }
}
