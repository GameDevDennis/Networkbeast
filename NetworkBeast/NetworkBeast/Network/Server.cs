﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NetworkBeast.GameLogic;
using NetworkBeast.NetworkSending;
using NetworkBeast.PackageHandlers;


namespace NetworkBeast.Network
{
    class Server
    {
        public static Server Instance;

        private Socket m_ServerSocket;

        private PackageHandling m_PacketHandler;
        private DataHandler m_DataHandler;

        private NetworkSender m_NetworkSender;
        private NetworkSendingQueue m_SendingQueue;
        private ServerSendingLoop m_SendingLoop;

        private VerificationLogic m_Verification;

        private Dictionary<Socket, Client> m_ConnectedClients;
        private Thread m_PackageHandlingThread;

        private bool m_IsRunning;

        public PackageHandling PacketHandler { get => m_PacketHandler; set => m_PacketHandler = value; }
        internal Dictionary<Socket, Client> ConnectedClients { get => m_ConnectedClients; set => m_ConnectedClients = value; }

        public void InitiliazeServer()
        {
            LogDebug.LoadDebug(this);
            Instance = this;
            ConnectedClients = new Dictionary<Socket, Client>();

            PacketHandler = new PackageHandling();
            m_DataHandler = new DataHandler();
            m_Verification = new VerificationLogic();
            m_SendingQueue = new NetworkSendingQueue();
            m_NetworkSender = new NetworkSender(m_SendingQueue);
            m_SendingLoop = new ServerSendingLoop(this,m_SendingQueue);

            m_ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_ServerSocket.Bind(new IPEndPoint(IPAddress.Any, 5555));
            m_ServerSocket.Listen(20);
            m_ServerSocket.BeginAccept(new AsyncCallback(AcceptConnection), null);

            m_IsRunning = true;

            m_PackageHandlingThread = new Thread(ServerPacketHandlingLoop);
            m_PackageHandlingThread.Start();

            LogDebug.DebugMessage("Server is running...", LogDebug.MessageType.Status);
        }

        private void AcceptConnection(IAsyncResult ar)
        {
            Socket client = m_ServerSocket.EndAccept(ar);
            Client clientCD = new Client(client, this);

            ConnectedClients.Add(client, clientCD);
            m_ServerSocket.BeginAccept(new AsyncCallback(AcceptConnection), null);
            LogDebug.DebugMessage("A new client has connected", LogDebug.MessageType.Warning);
            m_Verification.SetClientData(clientCD);
        }

        public void DisconnectPlayer(Socket socket)
        {
            ConnectedClients.Remove(socket);
            LogDebug.DebugMessage("A has disconnected", LogDebug.MessageType.Warning);
        }
        
        private void ServerPacketHandlingLoop()
        {
            while (m_IsRunning)
            {
                if (m_PacketHandler.HasPackets())
                {
                    ClientNetworkPackage package = m_PacketHandler.PackageQueue.Dequeue();

                    if (m_DataHandler.Packets.TryGetValue((int) package.Packet, out DataHandler.Packet packet))
                    {
                        packet?.Invoke(package);
                    }
                }

                Thread.Sleep(10);
            }
        }

        //TODO: Remove Debug Tools lateron
        private void SendDebugMessage(Socket socket)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt((int)ServerPacket.Debug);

            buffer.WriteString("test"); // Give player unique Game Token
            NetworkSender.SendToOne(buffer,socket);
        }

        private void SendCallBack(IAsyncResult ar)
        {
 
        }
    }
}
