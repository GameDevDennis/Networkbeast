﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace NetworkBeast.NetworkSending
{
    class NetworkSendingQueue
    {
        private Queue<SendingData> m_SendingQueue;

        public Queue<SendingData> SendingQueue { get => m_SendingQueue; set => m_SendingQueue = value; }

        public NetworkSendingQueue()
        {
            LogDebug.LoadDebug(this);
            SendingQueue = new Queue<SendingData>();
        }

        public void QueuePackage(byte[] data, Socket socket, PacketType type)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt(data.Length);
            buffer.WriteBytes(data);

            SendingData sendData = new SendingData
            {
                Buffer = buffer,
                Type = type,
                Receiver = socket,
                ByteLength = data.Length
            };
            SendingQueue.Enqueue(sendData);
        }

        public bool HasPackets()
        {
            if (SendingQueue.Count > 0)
                return true;
            else return false;
        }
    }

    public struct SendingData
    {
        public ByteBuffer Buffer;
        public PacketType Type;
        public Socket Receiver;
        public int ByteLength;
    }
}
