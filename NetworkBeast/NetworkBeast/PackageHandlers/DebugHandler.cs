﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetworkBeast.PackageHandlers
{
    class DebugHandler
    {
        internal void HandleDebug(ClientNetworkPackage package)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(package.Data);
            int size = buffer.ReadInt();
            int id = buffer.ReadInt();
            string msg = buffer.ReadString();
            LogDebug.DebugMessage(msg, LogDebug.MessageType.PacketDebug);
        }
    }
}
