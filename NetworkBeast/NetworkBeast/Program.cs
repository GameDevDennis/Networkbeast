﻿using System;
using System.Threading;
using NetworkBeast.Network;

namespace NetworkBeast
{
    class Program
    {
        static void Main(string[] args)
        {
            Server server = new Server();
            server.InitiliazeServer();
            Console.ReadKey();
        }
    }
}
