﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkBeastTypes;

namespace NetworkBeastData
{
    public class PlayerData
    {
        public bool IsLoggedin { get; set; }
        public string GameToken { get; set; }
        public int CharID { get; set; }
        public string Username { get; set; }
        public Vector3f Position { get; set; }

    }
}
