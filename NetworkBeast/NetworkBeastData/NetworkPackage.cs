﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;


public interface INetworkPackage
{

}

public struct ClientNetworkPackage : INetworkPackage
{
    public ClientPacket Packet { get; }
    public Socket Socket { get; }
    public byte[] Data { get; }
    
    public ClientNetworkPackage(ClientPacket packet, Socket socket, byte[] data)
    {
        Packet = packet;
        Socket = socket;
        Data = data;
    }
}
