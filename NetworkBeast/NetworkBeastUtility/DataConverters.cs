﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkBeastTypes;


public static class DataConverters
{
    public static byte[] ConvertVector3ToBytes(Vector3 v3)
    {
        byte[] bytes = new byte[12];
        Buffer.BlockCopy(bytes,0, BitConverter.GetBytes(v3.X),0 ,sizeof(int));
        Buffer.BlockCopy(bytes, 4, BitConverter.GetBytes(v3.Y), 0, sizeof(int));
        Buffer.BlockCopy(bytes, 8, BitConverter.GetBytes(v3.Z), 0, sizeof(int));

        return bytes;
    }

    public static byte[] ConvertVector3ToBytes(Vector3f v3)
    {
        byte[] bytes = new byte[12];
        Buffer.BlockCopy(bytes, 0, BitConverter.GetBytes(v3.X), 0, sizeof(float));
        Buffer.BlockCopy(bytes, 4, BitConverter.GetBytes(v3.Y), 0, sizeof(float));
        Buffer.BlockCopy(bytes, 8, BitConverter.GetBytes(v3.Z), 0, sizeof(float));

        return bytes;
    }

    public static Vector3 ConvertBytesToVector3(byte[] data)
    {
        Vector3 v3 = Vector3.zero;
        v3.X = BitConverter.ToInt32(data, 0 * sizeof(int));
        v3.Y = BitConverter.ToInt32(data, 1 * sizeof(int));
        v3.Z = BitConverter.ToInt32(data, 2 * sizeof(int));

        return v3;
    }

    public static Vector3f ConvertBytesToVector3f(byte[] data)
    {
        Vector3f v3 = Vector3f.zero;
        v3.X = BitConverter.ToSingle(data, 0 * sizeof(float));
        v3.Y = BitConverter.ToSingle(data, 1 * sizeof(float));
        v3.Z = BitConverter.ToSingle(data, 2 * sizeof(float));

        return v3;
    }
}
