﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class LogDebug
{
    public enum MessageType
    {
        Status,
        Warning,
        PacketDebug,
        Info
    }

    public static void LoadDebug(object myClass)
    {
        char split = '.';
        string[] fullName = myClass.ToString().Split(split);
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write(fullName[fullName.Length - 1] + ".CS");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine(" - has been loaded succesfully");
        Console.ResetColor();
    }

    public static void DebugMessage(string Msg, MessageType type)
    {
        switch (type)
        {
            case MessageType.Status:
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Status ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" - " + Msg);
                break;
            case MessageType.Warning:
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Warning ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" - " + Msg);
                break;

            case MessageType.PacketDebug:
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("PacketDebug");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" - " + Msg);
                break;
            case MessageType.Info:
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.Write("[INFO]");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" - " + Msg);
                break;
        }
    }

}
